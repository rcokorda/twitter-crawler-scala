package com.baktun.twittercrawler

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager
import org.apache.http.HttpHost
import org.apache.http.conn.routing.HttpRoute
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients

class HttpClientProvider(max: Int = 5) {
  val cm = new PoolingHttpClientConnectionManager();
  cm.setMaxTotal(max);
  cm.setDefaultMaxPerRoute(max);
  val twitter = new HttpHost("api.twitter.com", 80);
  val apiTwitterCom = new HttpHost("api.twitter.com", 80);    
  cm.setMaxPerRoute(new HttpRoute(apiTwitterCom), max);
  
  val httpClientBuilder = HttpClients.custom().setConnectionManager(cm);
  
  def getHttpClient() : CloseableHttpClient = httpClientBuilder.build();
}