package com.baktun.twittercrawler

import akka.actor.ActorRef
import scala.util.Try
 
sealed trait TwitterIOManagerMessage
final case class EnsureTwitterIO(authHeader: TwitterAuthHeader) extends TwitterIOManagerMessage

sealed trait TwitterIOMessage

final case class TwitterIOAvailable(authHeader: TwitterAuthHeader) extends TwitterIOMessage
final case class TwitterIOCreated(authHeader: TwitterAuthHeader) extends TwitterIOMessage
final case class TwitterIOUnavailable(authHeader: TwitterAuthHeader, reason: Any) extends TwitterIOMessage
final case class TwitterIONoAuthData(authHeader: TwitterAuthHeader) extends TwitterIOMessage
final case object Tick extends TwitterIOMessage

sealed trait AuthedTwitterIOMessage {
  def authHeader: Option[TwitterAuthHeader]
}

final case class PauseTwitterIO(reason: Any, authHeader: Option[TwitterAuthHeader] = None) 
  extends TwitterIOMessage with AuthedTwitterIOMessage
final case class ResumeTwitterIO(reason: String, authHeader: Option[TwitterAuthHeader] = None) 
  extends TwitterIOMessage with AuthedTwitterIOMessage
final case class StopTwitterIO(reason: Any, authHeader: Option[TwitterAuthHeader] = None) 
  extends TwitterIOMessage with AuthedTwitterIOMessage

//Here's the conflicting situation.
//You see: the constructor parameter authHeader is repeated in TwitterIOAvailable, TwitterIOCreated
//TwitterIOUnavailable, Search (below). Those messages happen to be subclass of TwitterIOMessage.
//So it's normal to ask: why not declaring authHeader as constructor parameter of TwitterIOMessage?
//Well, the thing is: there is a type of TwitterIOMessage that doesn't require authHeader (e.g.: Tick).
//So, no, I can't. What about multiple inherticance (with Trait)? Make a trait that holds the authHeader
//and make those TwitterIOAvailable, TwitterIOCreated, etc mix-in that trait.
//The problem is TwitterIOAvailable, TwitterIOCreated are case class. Meaning: the mention of 
//authHeader in its parameter list will automatically create a public field. 
//I guess it will clash with the authHeader declared in the Trait (?). Let's see....

final case class RestExecutionFailure(throwable : Throwable) extends TwitterIOMessage

abstract class TwitterOp(authHeader: Option[TwitterAuthHeader]) extends TwitterIOMessage 
with AuthedTwitterIOMessage {
  def twitterRequestUrl: String
  def buildResult(twitterResponse: TwitterResponse): TwitterOpResult
}

abstract class TwitterOpResult(val op: TwitterOp, twitterResponse: TwitterResponse) extends TwitterIOMessage {
  val error: Option[TwitterIOError] = {
    twitterResponse.header.error
  }
}

final case class Search(query: SearchQuery, authHeader: Option[TwitterAuthHeader]) extends TwitterOp(authHeader) {
  override def twitterRequestUrl: String = {
    s"https://api.twitter.com/1.1/search/tweets.json?${query.buildQueryParams()}"
  }
  
  override def buildResult(twitterResponse: TwitterResponse): TwitterOpResult = {
    new SearchResult(this, twitterResponse)
  }
}

final class SearchResult(op: Search, twitterResponse: TwitterResponse) 
extends TwitterOpResult(op, twitterResponse) {
  val (tweets, metadata, nextSearchMaxId): 
  (Option[List[Map[String, Any]]], Option[Map[String, Any]], Option[String]) = {
    import scala.collection.mutable
    val tweetsListBuffer = new mutable.ListBuffer[Map[String, Any]]()
    for {
      responseJson <- twitterResponse.json
      statuses <- responseJson.get("statuses")
      tweet <- statuses.asInstanceOf[List[Map[String, Any]]]
    } tweetsListBuffer.append(tweet)
    
    var nextSearchMaxId: Option[String] = None
    var metadata: Option[Map[String, Any]] = None
    
    for {
      responseJson <- twitterResponse.json
      mData <- responseJson.get("search_metadata")      
    } {
      metadata = Some(mData.asInstanceOf[Map[String, Any]])
      
      for {
        nextQueryParams <- mData.asInstanceOf[Map[String, Any]].get("next_results")
      } {
        import scala.util.matching.Regex
        val pattern = ".*max_id=([0-9]+)?-*?".r
        pattern.findAllIn(nextQueryParams.asInstanceOf[String]).matchData foreach(m => {
          nextSearchMaxId = Some(m.group(1))
        })        
      }
    }
    
    if (tweetsListBuffer.isEmpty) {
      (None, metadata, nextSearchMaxId)
    } else {
      (Some(tweetsListBuffer.toList), metadata, nextSearchMaxId)
    }
  }
}

//about the use of sealed trait for akka: http://stackoverflow.com/questions/6322437/should-actor-messages-extend-a-common-trait
//http://stackoverflow.com/questions/11203268/what-is-a-sealed-trait
//http://stackoverflow.com/questions/1991042/scala-traits-vs-abstract-classes
//decision, decision.... trait!
sealed trait TweetStoreMessage
final case class StoreTweet(tweet: Map[String, Any], searchQuery: Option[SearchQuery] = None) extends TweetStoreMessage
final case class StoreTweetResult(tweetId: String, error: String, existing: Option[Boolean]) extends TweetStoreMessage
final case class StoreTweeter(tweeter: Map[String, Any]) extends TweetStoreMessage
final case class StoreTweeterResult(tweeterId: String, error: String, existing: Option[Boolean]) extends TweetStoreMessage

sealed trait TwitterAuthStoreMessage
final case class GetTwitterAuthHeaders(authLookupSpec: TwitterAuthLookupSpec) extends TwitterAuthStoreMessage //this returns list of authDataHeaders or None
final case class GetTwitterAuthHeadersResponse(headers: Option[Set[TwitterAuthHeader]]) extends TwitterAuthStoreMessage //this returns list of authDataHeaders or None
final case class LeaseTwitterAuth(header: TwitterAuthHeader) extends TwitterAuthStoreMessage //this returns twitter auth data
final case class LeaseTwitterAuthResponse(data: Option[TwitterAuthData]) extends TwitterAuthStoreMessage    
final case class ReleaseTwitterAuthData(header: TwitterAuthHeader) extends TwitterAuthStoreMessage    

sealed trait TwitterCrawlerMessage
final case class TickleCrawler(reason: String) extends TwitterCrawlerMessage
final case class FlipSearchResultPage(maxId: Option[String], tweetHitCount: Int, tweeterHitCount: Int) extends TwitterCrawlerMessage {
  def makeSearch(currentSearch: Search): Option[Search] = {
    var nextSearch: Option[Search] = None
    
    for {
      mId <- maxId
    } {
      nextSearch = Some(currentSearch.copy(query = currentSearch.query.copy(maxId = Some(mId))))
    }    
      
    return nextSearch;
  }
}
final case class StopCrawler(reason: Any) extends TwitterCrawlerMessage
final case class PauseCrawler(reason: String) extends TwitterCrawlerMessage
final case class ResumeCrawler(reason: String) extends TwitterCrawlerMessage
final case class CrawlerUnavailable(reason: Any) extends TwitterCrawlerMessage

sealed trait TwitterCrawlerManagerMessage
final case class StartCrawlerManager(reason: String) extends TwitterCrawlerManagerMessage
final case class PauseCrawlerManager(reason: String) extends TwitterCrawlerManagerMessage
final case class ResumeCrawlerManager(reason: String) extends TwitterCrawlerManagerMessage
final case class StopCrawlerManager(reason: Any) extends TwitterCrawlerManagerMessage

object CountType extends Enumeration {
  type CountType = Value
  val Tweet, Tweeter = Value
}
final case class CrawlerCountUpdate(countType: CountType.CountType, count: (Int, Int))

/*
 * This is what I think I want: 
 * - A crawler that searches for "obama" in language "spanish" for geocode="california"
 * - This crawler should be different from the one that searches for "obama" in language "spanish" for geocode="newyork"
 * - And this should also be a separate crawler from the one that searches for "obama" in language "english"
 * 
 * So, search query determines identity of the crawler... Except for: ....
 * - max_id ? --> no..., we cannot use this because when when we navigate the pages in the search result, max_id will change (as we move to the next page, going back in time)
 *   ... Hmm.., except if we have a notion of "initialSearchQuery". Then we can use the all the fields of search query as determining factors for crawler's identity
 *   
 * - I guess initialSearchQuery is a good concept. Let's use it.
 * 
 * - Now, what about groupId (and the account name & app name)
 *   - What really matters here is the account name
 *     - Account name: if I logged in to twitter as "raka_eyes_on_prabowo" (an account specifically set-up to be friends of supporters of 'prabowo') ... will I get a different result when I search for "tax policy" using my other account "raka_eyes_on_jokowi" (whose friends are jokowi's supporters)?
 *       I haven't found a definitive answer so far. But it's safe to think the answer is "yes".
 *     - App name: I doubt it has any effect whatsoever to search result
 *     - GroupId? It's a notion specifict to our system only, to associate a client with its set of twitter accounts. No impact on search result.
 *   - So.., twitter account_name, should also be another determining factor for an identity of a crawler.
 *   
 * - Hum...
 */