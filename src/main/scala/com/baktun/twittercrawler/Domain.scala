package com.baktun.twittercrawler

import java.net.URLEncoder
import scala.util.control.Exception

//http://stackoverflow.com/questions/3687806/can-i-name-a-tuple-define-a-structure-in-scala-2-8
final case class TwitterAuthLookupSpec(groupId: String, 
  accountApp: Option[TwitterAccountApp] = None) {
  override def toString(): String = {
    if (accountApp.isDefined) {
      groupId + "." + accountApp.get
    } else {
      groupId
    }
  }
}

final case class TwitterAccountApp(accName: String, 
  appName: Option[String] = None) {
  override def toString(): String = {
    if (appName.isDefined) {
      accName + "." + appName.get
    } else {
      accName
    }
  }
}

final case class TwitterAuthHeader(accName: String, appName: String) 
final case class TwitterAuthData(header: TwitterAuthHeader, apiKey: String, apiSecret: String, 
    accessToken: String, accessTokenSecret: String, enabled: Boolean = true)

final case class TwitterResponse(header: TwitterResponseHeader, json: Option[Map[String, Any]] = None) {
	//TODO: consistency check
}
final case class TwitterResponseHeader(httpCode: Int, error: Option[TwitterIOError] = None)
final case class TwitterIOError(code: Int, message: String)

//A case for case-class: http://www.codecommit.com/blog/scala/case-classes-are-cool
case class SearchQuery(val q: String, val lang: Option[String] = None, 
  val geocode: Option[String] = None, val page: Option[Int] = None,
  val count: Option[Int] = Some(100), val until: Option[String] = None,
  val sinceId: Option[String] = None, val maxId: Option[String] = None) {
  
  //https://api.twitter.com/1.1/search/tweets.json?q=indonesia&geocode=es&lang=es&locale=es&result_type=es&count=es&until=es&since_id=es&max_id=es&include_entities=es&callback=es
  def buildQueryParams(): String = {
    var queryParams = s"q=${URLEncoder.encode(q)}"
    
    if (lang.isDefined) {
      queryParams = queryParams + s"&lang=${lang.get}" 
    } 
    
    if (geocode.isDefined) {
      queryParams = queryParams + s"&geocode=${geocode.get}"
    }
    
    if (page.isDefined) {
      queryParams = queryParams + s"&page=${page.get}"
    } 
    
    if (count.isDefined) {
      queryParams = queryParams + s"&count=${count.get}"
    } 
    
    if (until.isDefined) {
      queryParams = queryParams + s"&until=${until.get}"
    } 
    
    if (sinceId.isDefined) {
      queryParams = queryParams + s"&since_id=${sinceId.get}"
    } 
    
    if (maxId.isDefined) {
      queryParams = queryParams + s"&max_id=${maxId.get}"
    }  
    
    return queryParams
  }
  
  override def toString(): String = {
    var ret = q
    
    if (lang.isDefined) {
      ret = ret + "." + lang.get 
    } else {
      ret = ret + "."
    }
    
    if (geocode.isDefined) {
      ret = ret + "." + geocode.get 
    } else {
      ret = ret + "."
    }
    
    if (page.isDefined) {
      ret = ret + "." + page.get 
    } else {
      ret = ret + "."
    }
    
    if (count.isDefined) {
      ret = ret + "." + count.get 
    } else {
      ret = ret + "."
    }
    
    if (until.isDefined) {
      ret = ret + "." + until.get 
    } else {
      ret = ret + "."
    }
    
    if (sinceId.isDefined) {
      ret = ret + "." + sinceId.get 
    } else {
      ret = ret + "."
    }
    
    if (maxId.isDefined) {
      ret = ret + "." + maxId.get 
    } else {
      ret = ret + "."
    }
    
    ret
  }
}