package com.baktun.twittercrawler

import akka.actor.ActorPath
import akka.actor.ActorRef
import scala.collection.mutable
import scala.collection.immutable.Queue

//http://blog.michaelhamrah.com/2014/01/programming-akkas-finite-state-machines-in-scala/

sealed trait TwitterIOState
case object TwitterIOIdle extends TwitterIOState
case object TwitterIOActive extends TwitterIOState
case object TwitterIOPaused extends TwitterIOState
case object TwitterIOStopped extends TwitterIOState
case class TwitterIOStateData(queue: Queue[(TwitterOp, ActorRef)], vouchersLeft: Int, haltReason: Option[Any] = None)

sealed trait CrawlerState
case object CrawlerInitial extends CrawlerState
case object CrawlerWaitingSearchResult extends CrawlerState
case object CrawlerProcessingSearchResult extends CrawlerState
case object CrawlerWaitingForRetry extends CrawlerState
case object CrawlerPaused extends CrawlerState
case object CrawlerStopped extends CrawlerState

case class CrawlerStateData(currentOp: Option[TwitterOp] = None, 
  unprocessedOpResult: Option[TwitterOpResult] = None,
  haltReason: Option[Any] = None, tweetCount: (Int, Int) = (0, 0), tweeterCount: (Int, Int) = (0, 0))
    
sealed trait CrawlerManagerState
case object CrawlerManagerIdle extends CrawlerManagerState
case object CrawlerManagerWaitingAuthHeaders extends CrawlerManagerState
case object CrawlerManagerRunning extends CrawlerManagerState
case object CrawlerManagerPaused extends CrawlerManagerState
case object CrawlerManagerStopped extends CrawlerManagerState

case class CrawlerRuntimeData(crawler: ActorRef, state: CrawlerState, tweetCount: (Int, Int), 
  tweeterCount: (Int, Int))
case class CrawlerManagerStateData(crawlers: Map[String, CrawlerRuntimeData] = 
  Map.empty[String, CrawlerRuntimeData], haltReason: Option[Any] = None)