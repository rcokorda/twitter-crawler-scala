package com.baktun.twittercrawler.actor

import akka.actor.Props
import akka.actor.Actor
import akka.event.Logging
import com.baktun.twittercrawler.StoreTweet
import com.baktun.twittercrawler.StoreTweeter
import com.baktun.twittercrawler.StoreTweetResult
import com.baktun.twittercrawler.StoreTweeterResult
import com.lambdaworks.jacks.JacksMapper
import com.mongodb.casbah.Imports._
import com.mongodb.util.JSON
import akka.actor.ActorRef

class ComboTweetStore(normalTweetStore: ActorRef, sqlTweetStore: ActorRef) extends Actor {
  import context.dispatcher 
  
	val log = Logging(context.system, this)
	
	def receive = {
	    case task: StoreTweet => {
	      //sqlTweetStore ! task
	      normalTweetStore.tell(task, sender)
	    }
	    case task: StoreTweeter => {
	      //sqlTweetStore ! task
	      normalTweetStore.tell(task, sender)
	    }
	}
}

object ComboTweetStore {
  def props(normalTweetStore: ActorRef, sqlTweetStore: ActorRef): Props = Props(
    new ComboTweetStore(normalTweetStore, sqlTweetStore)) 
}