package com.baktun.twittercrawler.actor

import akka.actor.Actor
import akka.event.Logging
import akka.actor.ActorRef
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import akka.actor.ActorNotFound
import scala.concurrent.Future
import scala.util.Success
import scala.util.Failure
import akka.actor.InvalidActorNameException
import com.baktun.twittercrawler.Search
import akka.actor.Props
import com.baktun.twittercrawler.EnsureTwitterIO
import com.baktun.twittercrawler.LeaseTwitterAuth
import com.baktun.twittercrawler.LeaseTwitterAuthResponse
import com.baktun.twittercrawler.Search
import akka.util.Timeout
import com.baktun.twittercrawler.HttpClientProvider
import com.baktun.twittercrawler.TwitterIOAvailable
import com.baktun.twittercrawler.TwitterIONoAuthData
import com.baktun.twittercrawler.TwitterIOCreated
import com.baktun.twittercrawler.TwitterIOUnavailable
import com.baktun.twittercrawler.SearchResult
import akka.actor.FSM.SubscribeTransitionCallBack
import akka.actor.FSM.Transition
import com.baktun.twittercrawler.TwitterIOState
import com.baktun.twittercrawler.TwitterIOStopped
import com.baktun.twittercrawler.ReleaseTwitterAuthData
import com.baktun.twittercrawler.PauseTwitterIO
import com.baktun.twittercrawler.PauseTwitterIO
import com.baktun.twittercrawler.StopTwitterIO
import com.baktun.twittercrawler.ResumeTwitterIO
import com.baktun.twittercrawler.TwitterIOMessage
import com.baktun.twittercrawler.TwitterAuthHeader
import com.baktun.twittercrawler.AuthedTwitterIOMessage
import com.baktun.twittercrawler.TwitterOp

class TwitterIOManager(twitterAuthStore: ActorRef, httpClientProvider: HttpClientProvider,
rate: TwitterIORate) extends Actor {
  import akka.pattern.{ ask, pipe }
  import context.dispatcher //what the fuck why we need this?
  import scala.concurrent.duration._
  
  val log = Logging(context.system, this)
  
  def receive = {
    case task: EnsureTwitterIO => {
      //This is to create instance of TwitterIO on demand, based on 
      //the information contained in the authHeader
      val taskSender = sender
      
      val twitterIOName = "twitterio-" + task.authHeader.accName + 
          "." + task.authHeader.appName
          
      println("EnsureTwitterIO: " + twitterIOName);
          
      val twitterIO = context.child(twitterIOName)
      if (twitterIO.isDefined) {
        println("EnsureTwitterIO.a: " + twitterIOName);
        taskSender ! TwitterIOAvailable(task.authHeader)  
      } else {
        println("EnsureTwitterIO.b: " + twitterIOName);        
        val tweetAuthFut = twitterAuthStore.ask(LeaseTwitterAuth(task.authHeader))(15 seconds)
	      
	      tweetAuthFut.onSuccess {
          case LeaseTwitterAuthResponse(None) => {
            //It could be that the available field for the twitter account
            //in the database is set to false
            println("EnsureTwitterIO.LeaseTwitterAuthResponse(None): " + twitterIOName);
            taskSender ! TwitterIONoAuthData(task.authHeader)
          }
          case LeaseTwitterAuthResponse(Some(authData)) => {
            //There's an available twitter account for use in the database
            println("EnsureTwitterIO.LeaseTwitterAuthResponse(Some(authData)): " + twitterIOName);
            val twitterIO = context.actorOf(TwitterIO.props(authData, rate, httpClientProvider), twitterIOName)
        	  twitterIO ! SubscribeTransitionCallBack(self)
            taskSender ! TwitterIOCreated(task.authHeader)	
          }
	      }  
        
        tweetAuthFut.onFailure {
          case t => {
            println("An error has occured: " + t.getMessage)
            taskSender ! TwitterIONoAuthData(task.authHeader)
          }
        }
      }
    }
    
    case task @ (_:TwitterOp | _:PauseTwitterIO | _:StopTwitterIO | _:ResumeTwitterIO) => {   
      val taskSender = sender
      
      var authedTwitterIOMsg = task.asInstanceOf[AuthedTwitterIOMessage]
      var authHeader: TwitterAuthHeader = authedTwitterIOMsg.authHeader.get
      
      val twitterIOName = "twitterio-" + authHeader.accName + "." + authHeader.appName 
      println("twitterIOName: " + twitterIOName + " task: " + task)
      //RAKA: this is the problem, cannot get an instance of TwitterIO (because it has been stopped already
      //due to previous error)
      //we should not stop the TwitterIO, we just have to put it in Paused state
      val twitterIOFut = context.actorSelection(twitterIOName).resolveOne(Duration(15, TimeUnit.SECONDS))
      
      //This is the correct way. Use callback when we want side-effecting (instead of creating new Future by chaining)
	    twitterIOFut.onComplete {
        case Success(twitterIO) => {
          twitterIO.tell(task, taskSender)
	      }
	      case Failure(exc) => {
	        println("TwitterIOUnavailable for " + twitterIOName + " reason: " + exc.getMessage)
	        taskSender ! TwitterIOUnavailable(authHeader, exc)
	      }
      }
    }
    
    case "Voluntary Genocide" => {
      for {
        child <- context.children
      } {
        child ! "Suicide" 
      }
    }
    
    case transition: Transition[ActorRef] => {
      transition.to.asInstanceOf[TwitterIOState] match {
        case TwitterIOStopped =>  {
          println ("fsmref: " + transition.fsmRef)
          transition.fsmRef ! ReleaseTwitterAuthData
        }
        case _ => {
          //NADA (?)
        }
      }
    }
  }
}

object TwitterIOManager {
  def props(twitterAuthStore: ActorRef, httpClientProvider: HttpClientProvider, 
  rate: TwitterIORate): 
  Props = Props(new TwitterIOManager(twitterAuthStore, httpClientProvider, rate)) 
}