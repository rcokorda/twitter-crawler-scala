package com.baktun.twittercrawler.actor

import akka.actor.Props
import akka.event.Logging
import akka.actor.Actor
import com.baktun.twittercrawler.LeaseTwitterAuth
import com.baktun.twittercrawler.TwitterAuthData
import scala.collection.mutable
import com.baktun.twittercrawler.LeaseTwitterAuthResponse
import com.baktun.twittercrawler.TwitterAuthHeader
import com.baktun.twittercrawler.ReleaseTwitterAuthData
import com.baktun.twittercrawler.GetTwitterAuthHeaders
import com.baktun.twittercrawler.GetTwitterAuthHeadersResponse
import com.baktun.twittercrawler.TwitterAuthHeader
import com.baktun.twittercrawler.ReleaseTwitterAuthData

class TwitterAuthStore2(dbHost: String, dbName: String, dbPort: Option[Int] = None, 
    dbUser: Option[String] = None, dbPassword: Option[String] = None) extends Actor {
	val log = Logging(context.system, this)
	val twitterAuths = mutable.Map(
	    new TwitterAuthData(TwitterAuthHeader("jananuraga", "app_jana_1"),
	        "Psm5zrlcAeLd3O1sZt9EVNzXa", "GWa7luyvDqtwrtViKW9k6H7YZQARrDukNi5N6KUBmylvmr5taq", 
	        "39356773-57Tj4kdYlvynNwBGIqfA8MPhwpr4rjRlLXXbuFQNk", 
	        "l53QVofydAfta2KiHVwB4GO5ZmMCk5Cy3ur4zPEf3Ab1r") -> (Set("group_1", "group_2"), true, true),
	    new TwitterAuthData(TwitterAuthHeader("jananuraga", "app_jana_2"),
	        "Psm5zrlcAeLd3O1sZt9EVNzXa", "GWa7luyvDqtwrtViKW9k6H7YZQARrDukNi5N6KUBmylvmr5taq", 
	        "39356773-57Tj4kdYlvynNwBGIqfA8MPhwpr4rjRlLXXbuFQNk", 
	        "l53QVofydAfta2KiHVwB4GO5ZmMCk5Cy3ur4zPEf3Ab1r") -> (Set("group_1"), false, true)
	)
	
	def receive = {
	  case task: GetTwitterAuthHeaders => {
	    
	    var matchingEntries = twitterAuths.filter(_._2._2)
	    matchingEntries = matchingEntries.filter {
	      _._2._1.contains(task.authLookupSpec.groupId)
	    }
	    
	    if (task.authLookupSpec.accountApp.isDefined) {
	      matchingEntries = matchingEntries
	      .filter {
	        task.authLookupSpec.accountApp.get.accName == _._1.header.accName
	      } 
	      
	      if (task.authLookupSpec.accountApp.get.appName.isDefined) {
	        matchingEntries = matchingEntries
	        .filter {
	          task.authLookupSpec.accountApp.get.appName.get == _._1.header.appName
	        } 
	      } 
	    }
	    
	    var resp: Option[Set[TwitterAuthHeader]] = None
	    
	    if (matchingEntries.size > 0) {
	      val headerSet = matchingEntries.keySet.map(_.header).toSet
	      resp = Some(headerSet)
	    }
	    
	    sender() ! GetTwitterAuthHeadersResponse(resp)
	    
	  }
	  case task: LeaseTwitterAuth => {	
	    val matchingEntry = twitterAuths.find(entry => {
	      (entry._2._2 == true) &&
	      (entry._2._3 == true) &&
	      (entry._1.header.accName == task.header.accName) && 
	      (entry._1.header.appName == task.header.appName)
	    })
	     
	    matchingEntry match {
	      case Some(entry) => {
	        twitterAuths(entry._1) = (twitterAuths(entry._1)._1, twitterAuths(entry._1)._2, false)
	        sender() ! LeaseTwitterAuthResponse(Some(entry._1))
	      }
	      case None => sender() ! LeaseTwitterAuthResponse(None)
	    }
      }
	  case task: ReleaseTwitterAuthData => {
      val matchingEntry = twitterAuths.find(_._1.header == task.header)
      matchingEntry match {
	      case Some(entry) => {
	        twitterAuths(entry._1) = (twitterAuths(entry._1)._1, twitterAuths(entry._1)._2, true)
	      }
	    }
    }		
	}   
}

object TwitterAuthStore2 {
  def props(dbHost: String, dbName: String, dbPort: Option[Int] = None, 
      dbUser: Option[String] = None, dbPassword: Option[String] = None): Props = Props(
          new TwitterAuthStore2(dbHost, dbName, dbPort, dbUser, dbPassword))
}