package com.baktun.twittercrawler.actor

import scala.collection.mutable
import com.baktun.twittercrawler.CrawlerPaused
import com.baktun.twittercrawler.CrawlerProcessingSearchResult
import com.baktun.twittercrawler.CrawlerState
import com.baktun.twittercrawler.CrawlerStateData
import com.baktun.twittercrawler.CrawlerStopped
import com.baktun.twittercrawler.CrawlerWaitingSearchResult
import com.baktun.twittercrawler.FlipSearchResultPage
import com.baktun.twittercrawler.PauseCrawler
import com.baktun.twittercrawler.Search
import com.baktun.twittercrawler.SearchQuery
import com.baktun.twittercrawler.SearchResult
import com.baktun.twittercrawler.StopCrawler
import com.baktun.twittercrawler.StoreTweet
import com.baktun.twittercrawler.StoreTweeter
import com.baktun.twittercrawler.TickleCrawler
import com.baktun.twittercrawler.TwitterAuthHeader
import com.baktun.twittercrawler.TwitterOp
import com.baktun.twittercrawler.TwitterOpResult
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.FSM
import akka.actor.Props
import akka.actor.actorRef2Scala
import com.baktun.twittercrawler.CrawlerInitial
import com.baktun.twittercrawler.StoreTweetResult
import com.baktun.twittercrawler.StoreTweeterResult
import com.baktun.twittercrawler.RestExecutionFailure
import com.baktun.twittercrawler.CrawlerWaitingForRetry
import com.baktun.twittercrawler.TwitterIOUnavailable
import com.baktun.twittercrawler.PauseCrawler
import com.baktun.twittercrawler.PauseCrawler
import com.baktun.twittercrawler.Search
import com.baktun.twittercrawler.Search
import com.baktun.twittercrawler.CrawlerUnavailable
import com.baktun.twittercrawler.CrawlerCountUpdate
import com.baktun.twittercrawler.CountType
import akka.actor.Cancellable
import com.baktun.twittercrawler.Instrumented

class TwitterCrawler(initialQuery: SearchQuery, authHeader: TwitterAuthHeader, 
  twitterIOManager: ActorRef, tweetStore: ActorRef, manager: ActorRef) 
  extends Actor with FSM[CrawlerState, CrawlerStateData] with Instrumented {	
  import context.dispatcher
  import akka.pattern.{ ask, pipe }
  import scala.concurrent.duration._
  
  metrics.gauge(self.path.toStringWithoutAddress, "tweetHits") {
    stateData.tweetCount._1
  }
  metrics.gauge(self.path.toStringWithoutAddress, "newTweets") {
    stateData.tweetCount._2
  }
  metrics.gauge(self.path.toStringWithoutAddress, "tweeterHits") {
    stateData.tweeterCount._1
  }
  metrics.gauge(self.path.toStringWithoutAddress, "newTweeters") {
    stateData.tweeterCount._2
  }
  
  startWith(CrawlerInitial, new CrawlerStateData(Some(Search(initialQuery, Some(authHeader)))))
      
  //the response can come when it'a paused, stopping, and stopped
  //what to do on that case... ? Well, we depends
  //when it's paused, we save it, without changing state
  //when it's stopping and stopped, we ignore it.  
  
  when(CrawlerInitial) {
    case Event(TickleCrawler(reason), CrawlerStateData(Some(search: Search), None, None, _, _)) => {
      goto(CrawlerWaitingSearchResult)
    }
  }
  
  when(CrawlerWaitingSearchResult) {
    case Event(searchResult: SearchResult, CrawlerStateData(Some(search: Search), None, None, _, _)) => {
      //do process search result  
      
      if (searchResult.error.isEmpty) {
        goto(CrawlerProcessingSearchResult) using stateData.copy(unprocessedOpResult=Some(searchResult));        
      } else {
        //act according to the error code        
        //if the error is because of twitter limiting api, go to paused, wait for message 
        //published by the twitterIO (about it's availability)
        //if the error is for network issue, pause
        val error = searchResult.error.get
        val temporary = Set(88, 130, 131).contains(error.code) 
        
        if (temporary) {
          goto(CrawlerWaitingForRetry) using stateData.copy(haltReason = searchResult.error)
        } else {
          //goto(CrawlerPaused) using stateData.copy(haltReason = searchResult.error)
          goto(CrawlerWaitingForRetry) using stateData.copy(haltReason = searchResult.error)
        }
      }
    }
    
    case Event(restFailure: RestExecutionFailure, CrawlerStateData(Some(twitterOp), None, None, _, _)) => {
      //do process search result    
      println("there was a failure " + restFailure.throwable.getMessage())
      //goto(CrawlerPaused) using stateData.copy(haltReason = Some(restFailure))
      goto(CrawlerWaitingForRetry) using stateData.copy(haltReason = Some(restFailure))
    }
    
    case Event(twitterIOUnavail: TwitterIOUnavailable, CrawlerStateData(Some(twitterOp), None, None, _, _)) => {
      //The corresponding TwitterIO is not available, no reason to stay on running. Go to paused then.
      println("TwitterCrawler.twitterIOUnavail: " + twitterIOUnavail)
      //goto(CrawlerPaused) using stateData.copy(haltReason = Some(twitterIOUnavail))
      goto(CrawlerWaitingForRetry) using stateData.copy(haltReason = Some(twitterIOUnavail))
    }
  }
  
  when(CrawlerProcessingSearchResult) {
    case Event(flip : FlipSearchResultPage, CrawlerStateData(Some(search: Search), 
      Some(searchResult: SearchResult), None, (tweetHits, newTweets), (tweeterHits, newTweeters))) => {
      //http://stackoverflow.com/questions/949419/scala-collection-map-cannot-be-added-to
        
      println ("Let's go to the next page " + flip.maxId)
      
      val newTweetCount = (tweetHits + flip.tweetHitCount, newTweets)
      val newTweeterCount = (tweeterHits + flip.tweeterHitCount, newTweeters)
      
      manager ! CrawlerCountUpdate(CountType.Tweet, newTweetCount)
      manager ! CrawlerCountUpdate(CountType.Tweeter, newTweeterCount)
      
      goto(CrawlerWaitingSearchResult) using stateData.copy(
        currentOp = flip.makeSearch(search), unprocessedOpResult = None, 
        tweetCount = newTweetCount, tweeterCount = newTweeterCount)
    }    
  }  
  
  when(CrawlerWaitingForRetry) {
    case Event("Retry", CrawlerStateData(Some(twitterOp), None, Some(error), _, _)) => {
      goto(CrawlerWaitingSearchResult) using stateData.copy(unprocessedOpResult = None, haltReason = None)
    }
  }
  
  when(CrawlerPaused) {
    case Event(TickleCrawler(reason), CrawlerStateData(Some(search: Search), Some(unprocessedOpResult), _, _, _)) => {
      goto(CrawlerProcessingSearchResult) using stateData.copy(haltReason = None)
    }
    case Event(TickleCrawler(reason), CrawlerStateData(Some(search: Search), None, _, _, _)) => {
      goto(CrawlerWaitingSearchResult) using stateData.copy(haltReason = None)
    }    
    case Event(opResult: TwitterOpResult, CrawlerStateData(Some(twitterOp), None, Some(PauseCrawler(reason)), _, _)) => 
      stay using stateData.copy(unprocessedOpResult = Some(opResult))    
    case Event(flip: FlipSearchResultPage, CrawlerStateData(Some(search: Search), Some(searchResult: SearchResult), None, _, _)) => 
      stay using stateData.copy(currentOp = flip.makeSearch(search), unprocessedOpResult = None)
  }  
  
  when(CrawlerStopped) {
    case Event(msg, _) => {
      sender ! CrawlerUnavailable(stateData.haltReason)
      stay
    }
  }
    
  whenUnhandled {
    case Event(cmd: PauseCrawler, _:CrawlerStateData) => 
      goto(CrawlerPaused) using stateData.copy(haltReason = Some(cmd.reason))
    case Event(cmd: StopCrawler, _: CrawlerStateData) => {
      goto(CrawlerStopped) using stateData.copy(haltReason = Some(cmd.reason))
    }
    case Event(storeTweetResult: StoreTweetResult, CrawlerStateData(_, _, _, 
      (tweetHits, newTweets), _)) => {
      
      if (storeTweetResult.error == null) {
        manager ! CrawlerCountUpdate(CountType.Tweet, (tweetHits, newTweets + 1))
        stay using stateData.copy(tweetCount = (tweetHits, newTweets + 1))
      } else {
        log.warning("Error storing tweet " + storeTweetResult.tweetId + ": " + storeTweetResult.error)
        stay
      }
    }  
    case Event(storeTweeterResult: StoreTweeterResult, CrawlerStateData(_, _, _, 
      _, (tweeterHits, newTweeters))) => {
      
      if (storeTweeterResult.error == null) {
        manager ! CrawlerCountUpdate(CountType.Tweeter, (tweeterHits, newTweeters + 1))
        stay using stateData.copy(tweeterCount = (tweeterHits, newTweeters + 1))
      } else {
        log.warning("Error storing tweeter " + storeTweeterResult.tweeterId + ": " + storeTweeterResult.error)
        stay
      }
    }     
  }
  
  //Scala multiple type pattern matching
  onTransition {
    case CrawlerWaitingSearchResult -> CrawlerWaitingForRetry => {
      //here actually we can make the wait variable, depending on the halt reason
      println("Entering CrawlerWaitingForRetry, reason for halt: " + nextStateData.haltReason)
      context.system.scheduler.scheduleOnce(15 seconds, self, "Retry")
    }
    
    case _ -> CrawlerWaitingSearchResult => {
      if (nextStateData.currentOp.isEmpty) {
        self ! StopCrawler("No more op")
      } else {
        twitterIOManager ! nextStateData.currentOp.get        
      }
    }
    
    case (CrawlerWaitingSearchResult | CrawlerPaused) -> CrawlerProcessingSearchResult => {
      val searchResult = nextStateData.unprocessedOpResult.get.asInstanceOf[SearchResult]
      
      var tweetHitCount = 0
      var tweeterHitCount = 0
      for {
        tweets <- searchResult.tweets
        tweet <- tweets
      } {       
        val tweetBreakdown = TweetBreakdown(tweet)
        var searchQuery: Option[SearchQuery] = None
        if (nextStateData.currentOp.get.isInstanceOf[Search]) {
          searchQuery = Some(nextStateData.currentOp.get.asInstanceOf[Search].query)
        }
        
        tweetStore ! StoreTweet(tweetBreakdown.status, searchQuery)
        tweetStore ! StoreTweeter(tweetBreakdown.user)
        tweetHitCount += 1
        tweeterHitCount += 1
        
        tweetBreakdown.retweetedStatus.foreach {
          tweetHitCount += 1
          tweetStore ! StoreTweet(_, searchQuery)
        }
        tweetBreakdown.retweetedUser.foreach {
          tweeterHitCount += 1
          tweetStore ! StoreTweeter(_)
        }
      }
      
      self ! FlipSearchResultPage(searchResult.nextSearchMaxId, tweetHitCount, tweeterHitCount)
    }   
  }	
}

object TwitterCrawler {
  def props(initialQuery: SearchQuery, authHeader: TwitterAuthHeader, 
    twitterIOManager: ActorRef, tweetStore: ActorRef, manager: ActorRef): Props = 
    Props(new TwitterCrawler(initialQuery, authHeader, twitterIOManager, tweetStore, manager)) 
}

class TweetBreakdown(val status: Map[String, Any], val user: Map[String, Any],
    val retweetedStatus: Option[Map[String, Any]] = None, 
    val retweetedUser: Option[Map[String, Any]] = None, 
    val additionalStatusIds: Option[Set[String]] = None, 
    val additionalUserIds: Option[Set[String]] = None)
    
object TweetBreakdown {
  def apply(wholeTweet: Map[String, Any]): TweetBreakdown = {
    import scala.collection.mutable
    
    var status: Map[String, Any] = wholeTweet
    var user: Map[String, Any] = null
    var retweetedStatus: Option[Map[String, Any]] = None
    var retweetedUser: Option[Map[String, Any]] = None
    var addlStatusIds = mutable.Set[String]()
    var addlUserIds = mutable.Set[String]()
    
    wholeTweet.get("in_reply_to_status_id_str").foreach(idObj => {
      if (idObj != null) addlStatusIds += idObj.asInstanceOf[String]
    })    
    
    wholeTweet.get("in_reply_to_user_id_str").foreach(idObj => {
      if (idObj != null) addlUserIds += idObj.asInstanceOf[String]
    })
    
    for {
      entities <- wholeTweet.get("entities")
      userMentions <- entities.asInstanceOf[Map[String, Any]].get("user_mentions") 
      userMention <- userMentions.asInstanceOf[List[Map[String, Any]]]
      idStr <- userMention.asInstanceOf[Map[String, Any]].get("id_str")
    } addlUserIds += idStr.asInstanceOf[String]    
    
    wholeTweet.get("retweeted_status").foreach(rObj => {
      var r = rObj.asInstanceOf[Map[String, Any]]
      
      r.get("in_reply_to_status_id_str").foreach(idObj => {
        if (idObj != null) addlStatusIds += idObj.asInstanceOf[String]
      })
      
      r.get("in_reply_to_user_id_str").foreach(idObj => {
        if (idObj != null) addlUserIds += idObj.asInstanceOf[String]
      })
      
      for {
        entities <- r.get("entities")
        userMentions <- entities.asInstanceOf[Map[String, Any]].get("user_mentions") 
        userMention <- userMentions.asInstanceOf[List[Map[String, Any]]]
        idStr <- userMention.asInstanceOf[Map[String, Any]].get("id_str")
      } addlUserIds += idStr.asInstanceOf[String]    
      
      r.get("user").foreach(rUserObj => {
        val rUser = rUserObj.asInstanceOf[Map[String, Any]]        
        r = r + ("user_id_str" -> rUser.get("id_str").get)
        r = r + ("user_screen_name" -> rUser.get("screen_name").get)
        r = r + ("user_name" -> rUser.get("name").get)
      
        retweetedUser = Some(rUser)
      })
      r -= "user"
      
      retweetedStatus = Some(r)
      status = status + ("retweeted_status_id" -> r.get("id_str").get)
      status = status + ("retweeted_status_user_id" -> retweetedUser.get("id_str"))
      status = status + ("retweeted_status_user_screen_name" -> retweetedUser.get("screen_name"))
    }) 
    status = status - "retweeted_status"
    
    status.get("user").foreach(sUserObj => {
      user = sUserObj.asInstanceOf[Map[String, Any]]
      status = status + ("user_id_str" -> user.get("id_str").get)
      status = status + ("user_screen_name" -> user.get("screen_name").get)
      status = status + ("user_name" -> user.get("name").get)
    })
    status -= "user"
    
    new TweetBreakdown(status, user, retweetedStatus, retweetedUser, 
        if (addlStatusIds.isEmpty) None else Some(addlStatusIds.toSet), 
        if (addlUserIds.isEmpty) None else Some(addlUserIds.toSet))
  }
}
