package com.baktun.twittercrawler.actor

import akka.actor.Props
import akka.event.Logging
import akka.actor.Actor
import com.baktun.twittercrawler.LeaseTwitterAuth
import com.baktun.twittercrawler.TwitterAuthData
import scala.collection.mutable
import com.baktun.twittercrawler.LeaseTwitterAuthResponse
import com.baktun.twittercrawler.TwitterAuthHeader
import com.baktun.twittercrawler.ReleaseTwitterAuthData
import com.baktun.twittercrawler.GetTwitterAuthHeaders
import com.baktun.twittercrawler.GetTwitterAuthHeadersResponse
import com.baktun.twittercrawler.TwitterAuthHeader
import com.baktun.twittercrawler.ReleaseTwitterAuthData
import com.mongodb.casbah.Imports._
import com.baktun.twittercrawler.TwitterAccountApp
import com.baktun.twittercrawler.LeaseTwitterAuthResponse

class TwitterAuthStore(dbHost: String, dbName: String, dbPort: Option[Int] = None, 
    dbUser: Option[String] = None, dbPassword: Option[String] = None) extends Actor {
	val log = Logging(context.system, this)
	val mongoClient = MongoClient("127.0.0.1", 27017)
  val db = mongoClient("semantio")
  val researchGroupsColl = db("research_groups")
  val twitterAuthsColl = db("twitter_auths")
	
	def receive = {
	  case task: GetTwitterAuthHeaders => {
	    var resp: Option[Set[TwitterAuthHeader]] = None
	    val q = MongoDBObject("name" -> task.authLookupSpec.groupId)
	    val researchGroup = researchGroupsColl.findOne(q)
	    
	    researchGroup.foreach {rg =>
        if (rg.containsField("twitter_auths")) {
          val tas = rg.get("twitter_auths").asInstanceOf[BasicDBList]
          val nameApps = (0 to tas.size() - 1).map {idx =>
            val ta = tas.get(idx).asInstanceOf[BasicDBObject]
            var ret: Option[DBObject] = None
            task.authLookupSpec.accountApp match {
              case None => {
                ret = Some(MongoDBObject("user_name" -> ta.get("user_name"), "app_name" -> ta.get("app_name")))
              }
              case Some(TwitterAccountApp(accName, None)) => {
                if (accName.equals(ta.get("user_name"))) {
                  ret = Some(MongoDBObject("user_name" -> ta.get("user_name"), "app_name" -> ta.get("app_name")))
                } 
              }
              case Some(TwitterAccountApp(accName, Some(appName))) => {
                if (accName.equals(ta.get("user_name")) &&
                    appName.equals(ta.get("app_name"))) {
                  ret = Some(MongoDBObject("user_name" -> ta.get("user_name"), "app_name" -> ta.get("app_name")))
                } 
              }
            } 
           
            ret
          }.filter(_.isDefined)
         
          if (nameApps.size > 0) {
            val q = MongoDBObject("enabled" -> true, "$or" -> nameApps) 
            val twitterAuths = twitterAuthsColl.find(q, MongoDBObject("user_name" -> 1, "app_name" -> 1, 
              "api_key" -> 1, "api_secret" -> 1))
            val matchedHeaders = twitterAuths.toSet[DBObject].map {x =>
              TwitterAuthHeader(x.get("user_name").asInstanceOf[String], x.get("app_name").asInstanceOf[String])
            }
            resp = Some(matchedHeaders)
          }     
        }
      }
    	    
	    sender ! GetTwitterAuthHeadersResponse(resp)
	  }
	  
	  case task: LeaseTwitterAuth => {	
	    var data: Option[TwitterAuthData] = None
	    /*
	    val q = MongoDBObject("enabled" -> true, "available" -> true, 
        "user_name" -> task.header.accName, "app_name" -> task.header.appName)
        * 
        */
	    //I think we don't need that available field
	    val q = MongoDBObject("enabled" -> true, 
        "user_name" -> task.header.accName, "app_name" -> task.header.appName)
        
      val twitterAuthComplete = twitterAuthsColl.findOne(q)
      twitterAuthComplete.foreach{tac => 
        data = Some(
          TwitterAuthData(TwitterAuthHeader(tac.get("user_name").asInstanceOf[String], 
          tac.get("app_name").asInstanceOf[String]), 
          tac.get("api_key").asInstanceOf[String], tac.get("api_secret").asInstanceOf[String], 
          tac.get("token").asInstanceOf[String], tac.get("token_secret").asInstanceOf[String])
        )
        
        //twitterAuthsColl.update(q, MongoDBObject("$set" -> MongoDBObject("available" -> false)))
        //Let's forget about this
      }  
	    
	    sender ! LeaseTwitterAuthResponse(data)
    }
	  
	  case task: ReleaseTwitterAuthData => {
	    val q = MongoDBObject("enabled" -> true, "available" -> true, 
        "user_name" -> task.header.accName, "app_name" -> task.header.appName)
      twitterAuthsColl.update(q, MongoDBObject("$set" -> MongoDBObject("available" -> true)))
      sender ! "Success"
    }		
	}   
}

object TwitterAuthStore {
  def props(dbHost: String, dbName: String, dbPort: Option[Int] = None, 
      dbUser: Option[String] = None, dbPassword: Option[String] = None): Props = Props(
          new TwitterAuthStore(dbHost, dbName, dbPort, dbUser, dbPassword))
}