package com.baktun.twittercrawler.actor

import java.io.IOException
import scala.collection.immutable.Queue
import scala.concurrent.duration.FiniteDuration
import scala.math.min
import scala.util.Left
import scala.util.Right
import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import com.baktun.twittercrawler.HttpClientProvider
import com.baktun.twittercrawler.PauseTwitterIO
import com.baktun.twittercrawler.RestExecutionFailure
import com.baktun.twittercrawler.ResumeTwitterIO
import com.baktun.twittercrawler.StopTwitterIO
import com.baktun.twittercrawler.Tick
import com.baktun.twittercrawler.TwitterAuthData
import com.baktun.twittercrawler.TwitterIOActive
import com.baktun.twittercrawler.TwitterIOError
import com.baktun.twittercrawler.TwitterIOIdle
import com.baktun.twittercrawler.TwitterIOPaused
import com.baktun.twittercrawler.TwitterIOState
import com.baktun.twittercrawler.TwitterIOStateData
import com.baktun.twittercrawler.TwitterIOStopped
import com.baktun.twittercrawler.TwitterIOUnavailable
import com.baktun.twittercrawler.TwitterOp
import com.baktun.twittercrawler.TwitterResponse
import com.baktun.twittercrawler.TwitterResponseHeader
import com.lambdaworks.jacks.JacksMapper
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.FSM
import akka.actor.Props
import akka.actor.actorRef2Scala
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer
import akka.actor.PoisonPill

case class TwitterIORate(duration: FiniteDuration, numberOfCalls: Int)

class TwitterIO(authData: TwitterAuthData, rate: TwitterIORate, 
    httpClientProvider: HttpClientProvider) extends Actor with FSM[TwitterIOState, TwitterIOStateData]{
  startWith(TwitterIOIdle, TwitterIOStateData(Queue[(TwitterOp, ActorRef)](), rate.numberOfCalls))  
  
  when(TwitterIOIdle) {
    case Event(twitterOp: TwitterOp, _:TwitterIOStateData) => {
    	goto(TwitterIOActive) using deliverOps(stateData.copy(queue = Queue((twitterOp, sender))))
    }
  }
 
  when(TwitterIOActive) {
    // Queue a message (when we cannot send messages in the
	  // current period anymore)
    case Event(twitterOp: TwitterOp, TwitterIOStateData(queue, 0, _)) => {  
      stay using stateData.copy(queue = queue.enqueue((twitterOp, sender)))
    }
    // Queue a message (when we can send some more messages
    // in the current period)
    case Event(twitterOp: TwitterOp, TwitterIOStateData(queue, _, _)) => {
      stay using deliverOps(stateData.copy(queue = queue.enqueue((twitterOp, sender)))) 
    }
    // Period ends and we have no more messages
    case Event(Tick, TwitterIOStateData(Seq(), _, _)) =>
      goto(TwitterIOIdle)
 
    // Period ends and we get more occasions to send messages
    case Event(Tick, _:TwitterIOStateData) =>
      stay using deliverOps(stateData.copy(vouchersLeft = rate.numberOfCalls))      
  }
  
  when(TwitterIOPaused) {
    case Event(ResumeTwitterIO(reason, None), TwitterIOStateData(Seq(), _, _)) => {
      goto(TwitterIOIdle) using stateData.copy(haltReason = None)
    } 
    case Event(ResumeTwitterIO(reason, None), _:TwitterIOStateData) => {
      goto(TwitterIOActive) using stateData.copy(haltReason = None)
    } 
  }
  
  when(TwitterIOStopped) {
    case Event(twitterOp: TwitterOp, TwitterIOStateData(_, _, haltReason)) =>
      sender ! TwitterIOUnavailable(authData.header, haltReason)
      stay
  }
  
  whenUnhandled {
    case Event(PauseTwitterIO(reason, None), _:TwitterIOStateData) => {  
      goto(TwitterIOPaused) using stateData.copy(haltReason = Some(reason))
    } 
    case Event("Suicide", _) => { 
      self ! PoisonPill
      stay
    } 
  }
 
  onTransition {
    case TwitterIOIdle -> TwitterIOActive => setTimer("moreVouchers", Tick, rate.duration, true)
    case TwitterIOActive -> TwitterIOIdle => cancelTimer("moreVouchers")
    case _ -> TwitterIOPaused => cancelTimer("moreVouchers")
    case _ -> TwitterIOStopped => cancelTimer("moreVouchers")
  }
  
  private def deliverOps(stateData: TwitterIOStateData): TwitterIOStateData = {
    import scala.math.min
    val nrOfMsgToSend = min(stateData.queue.length, stateData.vouchersLeft)
    
    //Contract: there's only one message from particular crawler in the queue. 
    //Why? Because after sending a message, a crawler will go to "waiting for result".
    //Even in the presence of retry, there wouldn't be duplicate:
    //In the simplest case: the message would have been deleted from the queue,
    //the (same) message sent from retry wouldn't cause a duplicate.
    //In a more complex case, no problem either:
    /*
      0. search issued -> goes to waiting for result
      1. error
      2. waiting for retry (timer ticking)
      3. paused (timer still ticking)
      4. resumed (timer still ticking) - this block will be matched:
         case Event(TickleCrawler(reason), CrawlerStateData(Some(search: Search), None, _, _, _)) => {
           goto(CrawlerWaitingSearchResult) using stateData.copy(haltReason = None)
         }
         another search (the same one as the one sent at step 0) will be sent -> waiting for result   
         in the meantime, ticker is still ticking
      5. got the result (for the search request at step 4)
      6. timer sets off, retry event is sent to the crawler.
      7. the crawler is not in the "waiting for retry" state.
         so, this retry is discarded. nothing happens.

      Ok, so there's no problem actually.
      I can forgo this "cancelling" the timer thing. Less variables to keep track.
     */
    
    stateData.queue.take(nrOfMsgToSend).foreach {twitterOp =>    
      executeRestMethod(twitterOp._1.twitterRequestUrl) match {
        case Left(exception) => {
          exception.printStackTrace()
          twitterOp._2 ! RestExecutionFailure(exception)
        }
        case Right(response) => {
          twitterOp._2 ! twitterOp._1.buildResult(response)
        }
      }
    }
 
    stateData.copy(queue = stateData.queue.drop(nrOfMsgToSend), 
      vouchersLeft = stateData.vouchersLeft - nrOfMsgToSend)
  }
      
  private def executeRestMethod(url: String) : Either[IOException, TwitterResponse] = {
    val httpGet = new HttpGet(url)
    val oAuthConsumer = new CommonsHttpOAuthConsumer(authData.apiKey, authData.apiSecret)
    oAuthConsumer.setTokenWithSecret(authData.accessToken, authData.accessTokenSecret)
    oAuthConsumer.sign(httpGet)

    val httpClient = httpClientProvider.getHttpClient()
    var httpResponse: CloseableHttpResponse = null
    try {
      httpResponse = httpClient.execute(httpGet)
      val httpCode = httpResponse.getStatusLine().getStatusCode()
      val body = IOUtils.toString(httpResponse.getEntity().getContent())
      
	    var error: Option[TwitterIOError] = None
	    var json: Option[Map[String, Any]] = None
	    
	    if (httpCode == 200) {
	      json = Some(JacksMapper.readValue[Map[String, Any]](body))
	    } else {
	      import scala.collection.mutable
	     
	      val bodyJson = JacksMapper.readValue[Map[String, Any]](body)
	      
	      for {
		      errors <- bodyJson.get("errors")
	      } {
	        val errorMap = errors.asInstanceOf[List[Map[String, Any]]].head
	        
	        val code = errorMap.get("code").get.asInstanceOf[Int]
	        val message = errorMap.get("message").get.asInstanceOf[String]
	        error = Some(TwitterIOError(code, message))
	      }
	    }    
      
      httpGet.releaseConnection()
	    Right(new TwitterResponse(new TwitterResponseHeader(httpCode, error), json))    
    } catch {
      case ioe: IOException => {
        Left(ioe)
      }
    } finally {
      if (httpResponse != null) try {
        httpResponse.close()
      } catch {
        case e: Exception => println(e.getMessage())
      }
    }
  }
}

object TwitterIO {
  def props(authData: TwitterAuthData, rate: TwitterIORate, httpClientProvider: HttpClientProvider): 
  Props = Props(new TwitterIO(authData, rate, httpClientProvider)) 
}