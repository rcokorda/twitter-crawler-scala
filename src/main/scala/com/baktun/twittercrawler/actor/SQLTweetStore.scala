package com.baktun.twittercrawler.actor

import akka.actor.Props
import akka.actor.Actor
import akka.event.Logging
import com.baktun.twittercrawler.StoreTweet
import com.baktun.twittercrawler.StoreTweeter
import com.baktun.twittercrawler.StoreTweetResult
import com.baktun.twittercrawler.StoreTweeterResult
import com.lambdaworks.jacks.JacksMapper
import com.mongodb.casbah.Imports._
import com.mongodb.util.JSON
import java.util.HashMap
import com.baktun.twittercrawler.util.DataHandlerB
import java.util.Date
import java.util.ArrayList
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Locale

class SQLTweetStore(dbHost: String, dbName: String, dbPort: Option[Int] = None, 
      dbUser: Option[String] = None, dbPassword: Option[String] = None) extends Actor {
  import context.dispatcher 
  
	val log = Logging(context.system, this)
	val DF = new SimpleDateFormat ("EEE MMM dd HH:mm:ss Z yyyy", Locale.US)
	
	def receive = {
	    case task: StoreTweet => {
	      val tweetMap: java.util.Map[String, Any] = new HashMap()
	      
	      tweetMap.put("id", java.lang.Long.parseLong(task.tweet.get("id").get.toString))
	      tweetMap.put("userId", java.lang.Long.parseLong(task.tweet.get("user_id_str").get.asInstanceOf[String]))
	      tweetMap.put("userScreenName", task.tweet.get("user_screen_name").get.asInstanceOf[String])
	      tweetMap.put("userName", task.tweet.get("user_name").get.asInstanceOf[String])
	      tweetMap.put("createdAt", DF.parse(task.tweet.get("created_at").get.asInstanceOf[String]))
	      tweetMap.put("text", task.tweet.get("text").get.asInstanceOf[String])
	      tweetMap.put("source", task.tweet.get("source").get.asInstanceOf[String])
	      tweetMap.put("retweetCount", java.lang.Long.parseLong(task.tweet.get("retweet_count").get.toString))
	      
	      if (task.tweet.get("geo").get != null) {
	        val geo = task.tweet.get("geo").get.asInstanceOf[Map[String, Any]]
	        val coordinates = geo.get("coordinates").get.asInstanceOf[List[Double]]
	        tweetMap.put("geoLocationLatitude", coordinates(0))
	        tweetMap.put("geoLocationLongitude", coordinates(1))
	      } 
	      
	      if (task.tweet.get("retweeted_status_id") != None) {
	        tweetMap.put("isRetweet", true);
	        tweetMap.put("retweetedStatusId", 
	            java.lang.Long.parseLong(task.tweet.get("retweeted_status_id").get.asInstanceOf[String]))
	        tweetMap.put("retweetedUserId", 
	            java.lang.Long.parseLong(task.tweet.get("retweeted_status_user_id").get.asInstanceOf[String]))
	        tweetMap.put("retweetedUserScreenName", 
	            task.tweet.get("retweeted_status_user_screen_name").get.asInstanceOf[String])
	      } else {
	        tweetMap.put("isRetweet", false);
	      }
	      
	      tweetMap.put("inReplyToStatusId", java.lang.Long.parseLong("-1"));
	      tweetMap.put("inReplyToUserId", java.lang.Long.parseLong("-1"));
	      if (task.tweet.get("in_reply_to_status_id_str") != null && task.tweet.get("in_reply_to_status_id_str").get != null) {
	        tweetMap.put("inReplyToStatusId", 
	            java.lang.Long.parseLong(task.tweet.get("in_reply_to_status_id_str").get.asInstanceOf[String]))
	        tweetMap.put("inReplyToUserId", 
	            java.lang.Long.parseLong(task.tweet.get("in_reply_to_user_id_str").get.asInstanceOf[String]))
	        tweetMap.put("inReplyToScreenName", 
	            task.tweet.get("in_reply_to_screen_name").get.asInstanceOf[String])
	      }

	      tweetMap.put("isTruncated", task.tweet.get("truncated").get.asInstanceOf[Boolean])
	      tweetMap.put("isFavorited", task.tweet.get("favorited").get.asInstanceOf[Boolean])
	      /*if (task.tweet.get("possibly_sensitive") != None) {
	        tweetMap.put("isPossiblySensitive", task.tweet.get("possibly_sensitive").get.asInstanceOf[Boolean])
	      }*/
	      tweetMap.put("isPossiblySensitive", false)
	      
	      DataHandlerB.saveTweet(tweetMap, dbHost, dbPort.get, 
	          dbName, dbUser.get, dbPassword.get)
	      
	      val y = task.tweet.get("entities")
	      val x = task.tweet.get("entities").get
	      
	      val entitiesMap = task.tweet.get("entities").get.asInstanceOf[Map[String, Any]]
	      
	      val hashtags = entitiesMap.get("hashtags").get.asInstanceOf[List[Map[String, Any]]]
	      val hashtagTexts = hashtags.map(hashtag => hashtag.get("text").get.asInstanceOf[String])	      

	      val urls = entitiesMap.get("urls").get.asInstanceOf[List[Map[Any, Any]]]
	      val urlsText = urls.map(url => {
	        Array(url.get("url").get.asInstanceOf[String], url.get("expanded_url").get.asInstanceOf[String])
	      })	 

	      val mentions = entitiesMap.get("user_mentions").get.asInstanceOf[List[Map[String, Any]]]
	      val mentionsText = mentions.map(mention => {
	        Array(mention.get("id_str").get.asInstanceOf[String], 
	            mention.get("screen_name").get.asInstanceOf[String],
	            mention.get("name").get.asInstanceOf[String])
	      })		      
	      
	      if (hashtagTexts.length > 0) {
	        DataHandlerB.saveHashtags(task.tweet.get("id").get.asInstanceOf[Long],
	          hashtagTexts.toArray, dbHost, dbPort.get, 
	          dbName, dbUser.get, dbPassword.get)
	      }
	      
	      if (urlsText.length > 0) {
	        DataHandlerB.saveTweetURLs(task.tweet.get("id").get.asInstanceOf[Long],
	          urlsText.toArray, dbHost, dbPort.get, 
	          dbName, dbUser.get, dbPassword.get)
	      }
	      
	      if (mentionsText.length > 0) {
	        DataHandlerB.saveTweetMentions(task.tweet.get("id").get.asInstanceOf[Long],
	          mentionsText.toArray, dbHost, dbPort.get, 
	          dbName, dbUser.get, dbPassword.get)
	      }
	          
	      var existing: Option[Boolean] = None
	      sender ! StoreTweetResult(task.tweet("id_str").asInstanceOf[String], null, 
	        existing)
	    }
	    case task: StoreTweeter => {
	      val tweeterMap: java.util.Map[String, Any] = new HashMap()
	      
	      tweeterMap.put("id", java.lang.Long.parseLong(task.tweeter.get("id").get.toString)) //I don't know why it's storing tweet id in the userprofile table
	      tweeterMap.put("userId", java.lang.Long.parseLong(task.tweeter.get("id").get.toString))
	      tweeterMap.put("userScreenName", task.tweeter.get("screen_name").get.asInstanceOf[String])
	      tweeterMap.put("createdAt", DF.parse(task.tweeter.get("created_at").get.asInstanceOf[String]))
	      tweeterMap.put("userCreatedAt", DF.parse(task.tweeter.get("created_at").get.asInstanceOf[String]))
	      tweeterMap.put("userDescription", task.tweeter.get("description").get.asInstanceOf[String])
	      tweeterMap.put("userLang", task.tweeter.get("lang").get.asInstanceOf[String])
	      tweeterMap.put("userLocation", task.tweeter.get("location").get.asInstanceOf[String])
	      tweeterMap.put("userFavouritesCount", task.tweeter.get("favourites_count").get.asInstanceOf[Int])
	      tweeterMap.put("userFollowersCount", task.tweeter.get("followers_count").get.asInstanceOf[Int])
	      tweeterMap.put("userFriendsCount", task.tweeter.get("friends_count").get.asInstanceOf[Int])
	      tweeterMap.put("userName", task.tweeter.get("name").get.asInstanceOf[String])
	      tweeterMap.put("userTimeZone", task.tweeter.get("time_zone").get.asInstanceOf[String])
	      tweeterMap.put("userStatusesCount", task.tweeter.get("statuses_count").get.asInstanceOf[Int])
	      
	      DataHandlerB.saveUserProfile(tweeterMap, dbHost, dbPort.get, 
	          dbName, dbUser.get, dbPassword.get)
	      
	      var existing: Option[Boolean] = None
	      sender ! StoreTweeterResult(task.tweeter("id_str").asInstanceOf[String], null, 
	        existing)
	    }
	}
}


object SQLTweetStore {
  def props(dbHost: String, dbName: String, dbPort: Option[Int] = None, 
      dbUser: Option[String] = None, dbPassword: Option[String] = None): Props = Props(
          new SQLTweetStore(dbHost, dbName, dbPort, dbUser, dbPassword)) 
}