package com.baktun.twittercrawler.actor

import akka.actor.Props
import akka.actor.Actor
import akka.event.Logging
import com.baktun.twittercrawler.StoreTweet
import com.baktun.twittercrawler.StoreTweeter
import com.baktun.twittercrawler.StoreTweetResult
import com.baktun.twittercrawler.StoreTweeterResult
import com.lambdaworks.jacks.JacksMapper
import com.mongodb.casbah.Imports._
import com.mongodb.util.JSON
import com.mongodb.casbah.query.dsl.SetOnInsertOp
import com.mongodb.casbah.query.dsl.SetOp
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.query.dsl.SetOnInsertOp
import java.text.SimpleDateFormat
import java.util.Locale

class TweetStore(dbHost: String, dbName: String, dbPort: Option[Int] = None, 
      dbUser: Option[String] = None, dbPassword: Option[String] = None) extends Actor {
  import context.dispatcher 
  
	val log = Logging(context.system, this)
	
  val mongoClient = MongoClient(dbHost, dbPort.getOrElse(27017))
  val db = mongoClient(dbName)
  val tweetColl = db("tweets")
  val tweeterColl = db("tweeters")
  val searchHitsColl = db("search_hits")
  val trendingTopicsColl = db("trending_topics")
	
	def receive = {
	    case task: StoreTweet => {
	      
	      
	      val jsonString = JacksMapper.writeValueAsString[Map[String, Any]](task.tweet)	  
	      
	      val query = MongoDBObject("id_str" -> task.tweet.get("id_str").get.asInstanceOf[String])
	      val value = (JSON.parse(jsonString).asInstanceOf[BasicDBObject])
	       
	      val currentTs = System.currentTimeMillis
	      
	      val updateQuery = 
	        $setOnInsert("inserted_at" -> currentTs, 
	            "created_at_ts" -> TweetStore.toTimestamp(value.get("created_at").asInstanceOf[String])) ++
	        $set(value.append("updated_at", currentTs).toSeq: _*)
	       
	      val writeResult = tweetColl.update(query, updateQuery, upsert = true)
	      
	      var existing: Option[Boolean] = None
	      if (writeResult.getError() == null) {
	        existing = Some(writeResult.isUpdateOfExisting())
	      }
	      
	      task.searchQuery.foreach {sq =>
	        val query = MongoDBObject("q" -> sq.q, "tweet_id" -> task.tweet.get("id_str").get, 
	            "lang" -> sq.lang.getOrElse(null), "geocode" -> sq.geocode.getOrElse(null))
	            
	        val updateQuery = 
	          $setOnInsert("inserted_at" -> currentTs) ++
	          //$set((query ++  ("updated_at" -> currentTs)).toSeq: _*)
	          $set(query.toSeq: _*) 
	          //updated_at doesn't make sense for search_hit	          
	            
	        //http://docs.mongodb.org/manual/reference/operator/update/setOnInsert/
	        //{ $setOnInsert: { defaultQty: 100 } }
	            
	        searchHitsColl.update(query, updateQuery, upsert = true)
	      }
	      
	      sender ! StoreTweetResult(task.tweet("id_str").asInstanceOf[String], writeResult.getError(), 
	        existing)
	    }
	    case task: StoreTweeter => {
	      val jsonString = JacksMapper.writeValueAsString[Map[String, Any]](task.tweeter)	       
	      val query = MongoDBObject("id_str" -> task.tweeter.get("id_str").get.asInstanceOf[String])
	      val value = JSON.parse(jsonString).asInstanceOf[BasicDBObject]
	      
	      val currentTs = System.currentTimeMillis
	      val updateQuery = 
	          $setOnInsert("inserted_at" -> currentTs,
	            "created_at_ts" -> TweetStore.toTimestamp(value.get("created_at").asInstanceOf[String])) ++
	          $set(value.append("updated_at", currentTs).toSeq: _*)

	      val writeResult = tweeterColl.update(query, updateQuery, upsert = true)
	      
	      var existing: Option[Boolean] = None
	      if (writeResult.getError() == null) {
	        existing = Some(writeResult.isUpdateOfExisting())
	      }
	      
	      sender ! StoreTweeterResult(task.tweeter("id_str").asInstanceOf[String], writeResult.getError(), 
	        existing)
	    }
	}
}


object TweetStore {
  val DF = new SimpleDateFormat ("EEE MMM dd HH:mm:ss Z yyyy", Locale.US)
  val DFISO = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
  
  def props(dbHost: String, dbName: String, dbPort: Option[Int] = None, 
      dbUser: Option[String] = None, dbPassword: Option[String] = None): Props = Props(
          new TweetStore(dbHost, dbName, dbPort, dbUser, dbPassword)) 
          
  def toTimestamp(dateStr: String): Long = {
    DF.parse(dateStr).getTime()
  }
  
  def toTimestampISO(dateStr: String): Long = {
    DFISO.parse(dateStr).getTime()
  }
}