package com.baktun.twittercrawler.actor

import com.baktun.twittercrawler.CrawlerInitial
import com.baktun.twittercrawler.CrawlerManagerIdle
import com.baktun.twittercrawler.CrawlerManagerPaused
import com.baktun.twittercrawler.CrawlerManagerRunning
import com.baktun.twittercrawler.CrawlerManagerState
import com.baktun.twittercrawler.CrawlerManagerStateData
import com.baktun.twittercrawler.CrawlerManagerWaitingAuthHeaders
import com.baktun.twittercrawler.CrawlerState
import com.baktun.twittercrawler.GetTwitterAuthHeaders
import com.baktun.twittercrawler.GetTwitterAuthHeadersResponse
import com.baktun.twittercrawler.PauseCrawler
import com.baktun.twittercrawler.PauseCrawlerManager
import com.baktun.twittercrawler.SearchQuery
import com.baktun.twittercrawler.StartCrawlerManager
import com.baktun.twittercrawler.StopCrawlerManager
import com.baktun.twittercrawler.TickleCrawler
import com.baktun.twittercrawler.TwitterAuthHeader
import com.baktun.twittercrawler.TwitterAuthLookupSpec
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.FSM
import akka.actor.FSM.SubscribeTransitionCallBack
import akka.actor.FSM.Transition
import akka.actor.Props
import akka.actor.actorRef2Scala
import com.baktun.twittercrawler.StopCrawler
import com.baktun.twittercrawler.ResumeCrawlerManager
import com.baktun.twittercrawler.ResumeCrawler
import com.baktun.twittercrawler.CrawlerManagerStateData
import com.baktun.twittercrawler.CrawlerManagerStopped
import com.baktun.twittercrawler.CrawlerStopped
import com.baktun.twittercrawler.CrawlerRuntimeData
import com.baktun.twittercrawler.CrawlerCountUpdate
import com.baktun.twittercrawler.CountType
import com.baktun.twittercrawler.CrawlerPaused

//as a result of sending the the crawl message, I want to be informed of
//the crawler(s) that was(were) started / running.
//each crawler should be identified by auth_data + query
class TwitterCrawlerManager(authLookupSpec: TwitterAuthLookupSpec, query: SearchQuery, 
  twitterIOManager: ActorRef, twitterAuthStore: ActorRef, 
  tweetStore: ActorRef) extends Actor with FSM[CrawlerManagerState, CrawlerManagerStateData] {
  
  import context.dispatcher //what the fuck why we need this?
  import akka.pattern.{ ask, pipe }
  import scala.concurrent.duration._
  
  startWith(CrawlerManagerIdle, new CrawlerManagerStateData())
    
  when(CrawlerManagerIdle) {
    case Event(task: StartCrawlerManager, _:CrawlerManagerStateData) => {
      goto(CrawlerManagerWaitingAuthHeaders)
    }
  }
    
  //At this point, user would like to know how many instances of crawler were started
  //And monitor in real-time the progress of each one of them.
  when(CrawlerManagerWaitingAuthHeaders) {
    case Event(response: GetTwitterAuthHeadersResponse, _:CrawlerManagerStateData) => {
      //upon receiving list of twitter-accounts associated with this
      //crawler-manager, it spawns instances of crawler (each one bearing the name of )
      
      import scala.collection.mutable
      val crawlers = response.headers.fold(Map.empty[String, CrawlerRuntimeData]) {
        _.map(header => {
          //ALERT!: I guess this is the bug: assange-manager is sharing the same instances of
          //of crawler with snowden-manager
          //The question: is the sharing by design?
          //I guess what we really want is:
          //we don't want more than 1 instance of crawler doing crawl at the same time
          //(it would be a waste of resource)
          //Maybe query.toString would be sufficient to identify the crawler.
          //But we want to allow multiple instances of crawler doing the same query
          //as long as they are using different twitter account (with the hope that different
          //account would give different result).
          
          
          //Now the question is: what if the crawlermanager hits a crawler that is already stopped?
          
          //Hmm, look at this: the design does not allow the the sharing of crawlers....
          //It creates a new instance of crawler
         
          val crawlerName = "twittercrawler-" + header.accName + "." + header.appName
          val crawler = context.actorOf(TwitterCrawler.props(query, header, 
	          twitterIOManager, tweetStore, self), crawlerName)

	        println("TwitterCralwerManager " + self.path.toStringWithoutAddress + 
              " instance of crawler with name " + crawler.path.toStringWithoutAddress + " was created")
          crawlerName -> CrawlerRuntimeData(crawler, CrawlerInitial, (0, 0), (0, 0))
        }).toMap
      }
      
      if (crawlers.toList.length == 0) {
        goto(CrawlerManagerStopped) using stateData.copy(crawlers = crawlers, haltReason = Some("No matching auth info"))
      } else {
        goto(CrawlerManagerRunning) using stateData.copy(crawlers = crawlers) 
      }
    }
  } 
  
  when(CrawlerManagerRunning) {
    case Event(PauseCrawlerManager(reason), _:CrawlerManagerStateData) => {
      println("CrawlerManagerRunning:PauseCrawlerManager=" + reason)
      goto(CrawlerManagerPaused) using stateData.copy(haltReason = Some(reason))
    }
  }
  
  when(CrawlerManagerStopped) {
    case Event(msg, __CrawlerManagerStateData) => {
      sender ! CrawlerManagerStopped
      stay
    }
  }  
  
  when(CrawlerManagerPaused) {    
    case Event(cmd @ ResumeCrawlerManager(reason), _:CrawlerManagerStateData) => { 
      if (stateData.crawlers.size == 0) {
        goto(CrawlerManagerWaitingAuthHeaders) using stateData.copy(haltReason = None)
      } else {
        goto(CrawlerManagerRunning) using stateData.copy(haltReason = None)
      }
      
    }
  }  
    
  onTransition {
    case _ -> CrawlerManagerWaitingAuthHeaders => {
      twitterAuthStore ! GetTwitterAuthHeaders(authLookupSpec)
    }
    case CrawlerManagerRunning -> CrawlerManagerPaused => {
      //this crawler manager was told to Pause, so it has to forward this instruction
      //to all instances of crawlers belong to it.
      for {
        crawlerRuntimeData <- nextStateData.crawlers.values
      } {
        if (crawlerRuntimeData.state != CrawlerStopped &&
            crawlerRuntimeData.state != CrawlerPaused) {
          crawlerRuntimeData.crawler ! PauseCrawler(nextStateData.haltReason.get.asInstanceOf[String])
        }
      }
    }
    case _ -> CrawlerManagerStopped => {
      println("CrawlerManager " + self.path + " is stopped")
      //this crawler manager was told to Stop, so it has to forward this instruction
      //to all instances of crawlers belong to it.
      for {
        crawlerRuntimeData <- nextStateData.crawlers.values
      } {
        if (crawlerRuntimeData.state != CrawlerStopped) {
          crawlerRuntimeData.crawler ! StopCrawler(nextStateData.haltReason.get.asInstanceOf[String])
        } 
      }
    }
    case CrawlerManagerWaitingAuthHeaders -> CrawlerManagerRunning => {
      //There is only one possible way for this transition:
      //from the initial state.
      //So, there's only two things to do here: 
      //1. set up the callbacks (this controller will be listening to the transitions of the child crawlers)
      //2. 'tickle' the crawlers so they start running
      for {
        crawlerRuntimeData <- nextStateData.crawlers.values
      } {
        if (crawlerRuntimeData.state != CrawlerStopped) {
          crawlerRuntimeData.crawler ! SubscribeTransitionCallBack(self)
          crawlerRuntimeData.crawler ! TickleCrawler("Start") 
        }
      }
    }    
  }
  
  whenUnhandled {
    case Event(CrawlerCountUpdate(countType, count), _:CrawlerManagerStateData) => {
      val crawlerRuntimeData = stateData.crawlers(sender.path.name)
      
      //This manager will get the count updates (wither tweet / tweeter) 
      //from the the crawler instances
      //the manager will simply copy it (in its crawlerRuntimeData map)
      
      countType match {
        case CountType.Tweet => {
         val updatedCrawlers = stateData.crawlers + 
          (sender.path.name -> crawlerRuntimeData.copy(tweetCount = count))
          stay using stateData.copy(crawlers = updatedCrawlers)
        }
        case CountType.Tweeter => {
         val updatedCrawlers = stateData.crawlers + 
          (sender.path.name -> crawlerRuntimeData.copy(tweeterCount = count)) 
          stay using stateData.copy(crawlers = updatedCrawlers)
        }
      }
    }
    
    case Event(transition: Transition[ActorRef], _:CrawlerManagerStateData) => {      
      val crawlerRuntimeData = stateData.crawlers(transition.fsmRef.actorRef.path.name)
      val updatedCrawlers = stateData.crawlers + 
        (transition.fsmRef.actorRef.path.name -> crawlerRuntimeData.copy(
            state = transition.to.asInstanceOf[CrawlerState]))
         
      //This is the state transition listener, the one that will be receiving
      //state transition events from the crawlers            
      val stoppedCrawlersCount = updatedCrawlers.values.map {crd => 
        crd.state match {
          case CrawlerStopped => 1
          case _ => 0
        }
      }.sum
      
      //println("CrawlerManager " + self.path + " stoppedCrawlersCount: " + stoppedCrawlersCount)
      //println("CrawlerManager " + self.path + " updatedCrawlers.size: " + updatedCrawlers.size)
      if (stoppedCrawlersCount == updatedCrawlers.size) {
        //All the crawlers have been stopped, so this manager should stop as well
        goto(CrawlerManagerStopped) using stateData.copy(crawlers = updatedCrawlers, 
            haltReason = Some("All crawlers stopped"))
      } else {
        stay() using stateData.copy(crawlers = updatedCrawlers)
      }
    }  
    
    case Event(StopCrawlerManager(reason), _:CrawlerManagerStateData) => {
      //TODO: I'm not sure why did I put this in the whenUnhandled section?
      for {
        crawlerRuntimeData <- stateData.crawlers.values
      } {
        crawlerRuntimeData.crawler ! StopCrawler(reason)
      }
      
      goto(CrawlerManagerStopped) using stateData.copy(haltReason = Some(reason))
    }        
  }
}

object TwitterCrawlerManager {
  def props(authLookupSpec: TwitterAuthLookupSpec, query: SearchQuery, 
    twitterIOManager: ActorRef, twitterAuthStore: ActorRef, 
    tweetStore: ActorRef): Props = Props(new TwitterCrawlerManager(authLookupSpec, query, 
      twitterIOManager, twitterAuthStore, tweetStore)) 
}