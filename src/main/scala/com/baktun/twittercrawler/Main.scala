package com.baktun.twittercrawler

import akka.actor.{ActorSystem, Props, Actor}
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import com.baktun.twittercrawler.actor.TwitterIO
import scala.concurrent.impl.Future
import scala.concurrent.Future
import com.baktun.twittercrawler.actor.TwitterAuthStore
import scala.util.Success
import scala.util.Failure
import com.baktun.twittercrawler.actor.TwitterIOManager
import com.baktun.twittercrawler.actor.TwitterCrawlerManager
import com.baktun.twittercrawler.actor.TweetStore
import com.baktun.twittercrawler.actor.TwitterIORate
import java.util.concurrent.TimeUnit
import com.baktun.twittercrawler.actor.SQLTweetStore
import com.baktun.twittercrawler.actor.ComboTweetStore
import scala.collection.mutable.ListBuffer
import com.codahale.metrics.ConsoleReporter
import com.codahale.metrics.JmxReporter

object TwitterCrawlerMain extends App {  
  // Create the 'twitter-crawler' actor system
  val system = ActorSystem("twitter-crawler")
  val metricRegistry = new com.codahale.metrics.MetricRegistry()
  /*
  val reporter: ConsoleReporter = ConsoleReporter.forRegistry(metricRegistry)
    .convertRatesTo(TimeUnit.SECONDS)
    .convertDurationsTo(TimeUnit.MILLISECONDS)
    .build();*/
  val reporter: JmxReporter = JmxReporter.forRegistry(metricRegistry).build();
  reporter.start();
  
  import system.dispatcher //what the fuck why we need this?
  import scala.concurrent.duration._  
  import akka.actor.ActorDSL._
  
  val twitterAuthStore = system.actorOf(TwitterAuthStore.props("127.0.0.1", "semantio"))
  val twitterIOManager = system.actorOf(TwitterIOManager.props(
      twitterAuthStore, new HttpClientProvider(5), TwitterIORate(Duration(15, TimeUnit.SECONDS), 15)))
  
  implicit val timeout = Timeout(890 seconds)
  implicit val recv = inbox() (system)
  
  
  var headerToUse: TwitterAuthHeader = null
  
  val respFut = twitterAuthStore ? GetTwitterAuthHeaders(TwitterAuthLookupSpec("group_2"))
  respFut.onComplete {
    case Success(resp) => {
      resp match {
        case getAuthHeaderResp: GetTwitterAuthHeadersResponse => {
          for {
            headers <- getAuthHeaderResp.headers
            authHeader <- headers
          } {
            twitterIOManager ! EnsureTwitterIO(authHeader)
            headerToUse = authHeader
          }
        } 
      }  
	  }
	  case Failure(exc) => {
	    exc.printStackTrace()
	    //TODO: better handling
	  }
  }
  
  val respFut2 = twitterAuthStore ? GetTwitterAuthHeaders(TwitterAuthLookupSpec("group_4"))
  respFut2.onComplete {
    case Success(resp) => {
      resp match {
        case getAuthHeaderResp: GetTwitterAuthHeadersResponse => {
          for {
            headers <- getAuthHeaderResp.headers
            authHeader <- headers
          } {
            twitterIOManager ! EnsureTwitterIO(authHeader)
            headerToUse = authHeader
          }
        } 
      }  
	  }
	  case Failure(exc) => {
	    exc.printStackTrace()
	    //TODO: better handling
	  }
  }
  
  //TODO: implement semaphore mechanism, wait until all the twitterIOs started in the lines
  //above are all up. For now just sleep  
  Thread.sleep(10000)
  
  val sqlDbHost: String = args(0).asInstanceOf[String]
  val sqlDbName: String = args(1).asInstanceOf[String]
  val until: String = args(2).asInstanceOf[String]
  
  val namez: ListBuffer[String] = ListBuffer()
  args.drop(3).foreach(arg => namez += arg)
  
  val normalTweetStore = system.actorOf(TweetStore.props("127.0.0.1", "semantio"), "normal-tweet-store")
  val sqlTweetStore = system.actorOf(SQLTweetStore.props(sqlDbHost, sqlDbName, 
      Some(1433), Some("sa"), Some("Win4lt4d3n4152012")), "sql-tweet-store")
      
  val tweetStore = system.actorOf(ComboTweetStore.props(normalTweetStore, sqlTweetStore), "tweet-store")
  
  /*
  val names = Array("FERNANDO ELIZONDO ORTIZ", "FERNANDO MARGAIN BERLANGA", 
     "FELIPE DE JESUS CANTU RODRIGUEZ", "MARGARITA ARELLANES CERVANTES",
     "MAURICIO FERNANDEZ GARZA", "FEDERICO VARGAS RODRIGUEZ",
     "ABEL GUERRA GARZA", "CRISTINA DIAZ SALAZAR", "JAIME RODRIGUEZ CALDERON",
     "ILDEFONSO GUAJARDO VILLARREAL", "CRISTINA SADA SALINAS", "DIANA EUGENIA GONZALEZ SALDAÑA",
     "ALBERTO ANAYA GUTIERREZ", "LILIANA FLORES BENAVIDES", "MARTHA ZAMARRIPA RIVAS",
     "MAURICIO SADA SANTOS", "ROBERTO GALLARDO", "FERNANDO ELIZONDO BARRAGAN",
     "LUIS DONALDO COLOSIO RIOJAS", "TATIANA CLOUTHIER CARRILLO")
  */
  
  /*
  val names = Array("milenio.com", "@milenio", "animalpolitico", "aristeguinoticias.com", "@AristeguiOnline", 
      "@NoticiasMVS", "maspormas.com", "mexico.cnn.com", "@CNNMex", "@publimetromx", "publimetro.com.mx",
      "jornada.unam.mx", "@LaJornada");*/
  
  val names = namez.toArray
  var flag = true;
  for(i <- 0 until names.length){
    var group: String = null;
    if (flag) {
      group = "group_2";
    } else {
      group = "group_4";
    }
    
    flag = !flag;
    
    val query = SearchQuery(names(i), until = Some(until))
    val tcm = system.actorOf(TwitterCrawlerManager.props(TwitterAuthLookupSpec(group), 
        query, twitterIOManager, twitterAuthStore, tweetStore), 
      "twittercrawlermanager-" + Base62.hashSha1Base62DontPad(query.buildQueryParams()))
      tcm ! StartCrawlerManager("Launch it!")
  }
}

trait Instrumented extends nl.grons.metrics.scala.InstrumentedBuilder {
  val metricRegistry = TwitterCrawlerMain.metricRegistry
}

object Base62 {
  def hashSha1Base62DontPad(text: String): String = {
    val mdSha1 = java.security.MessageDigest.getInstance("SHA-1")
    val bytes = mdSha1.digest(text.getBytes("UTF-8"))
    val bigint = new java.math.BigInteger(1, bytes)
    val result = encodeInBase62(bigint)
    result
  }
  
  private val Base62Alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
  
  def encodeInBase62(number: BigInt): String = {
    // Base 62 is > 5 but < 6 bits per char.
    var result = new StringBuilder(capacity = number.bitCount / 5 + 1)
    var left = number
    do {
      val remainder = (left % 62).toInt
      left /= 62
      val char = Base62Alphabet.charAt(remainder)
      result += char
    }
    while (left > 0)
    result.toString
  }
}