package com.baktun.twittercrawler.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.Map;

public class DataHandlerB {
	
	private static Connection conn;
	
	private static java.sql.Connection getConnection(String host, int port, String dbName, String user, String password){
       try{
        	 if(conn==null) {
	             Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); 
	             conn= java.sql.DriverManager.getConnection("jdbc:sqlserver://" + host + ":" + port + ";DatabaseName=" + dbName, user, password);
	             
	             System.out.println("connecting to: " + "jdbc:sqlserver://" + host + ":" + port + ";DatabaseName=" + dbName);
	             
	             //Using for MARS search
	             //conn= java.sql.DriverManager.getConnection("jdbc:sqlserver://10.1.100.50:1433;DatabaseName=Baktunanalytics_Chile1","sa","Win4lt4d3n4152012");
	             if(conn!=null) System.out.println("Connection Successful!");
        	 }
       }catch(Exception e){
             e.printStackTrace();
             System.out.println("Error Trace in getConnection() : " + e.getMessage() + "\n " + e.getStackTrace().toString());
       }
        return conn;
    }
	
	public static void saveTweet(Map tweet, String host, int port, String dbName, String user, String password) {
		try {

			Connection cnx = getConnection(host, port, dbName, user, password);
			if(cnx==null || tweet==null)
				return;
			
			CallableStatement cs = cnx.prepareCall("{call TWITTER_STATUS2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cs.setLong(1, 		(Long) tweet.get("id")); 
			cs.setLong(2, 		(Long) tweet.get("userId"));
			cs.setString(3, 	(String) tweet.get("userScreenName")); 			
			cs.setTimestamp(4, 	new java.sql.Timestamp(((Date) tweet.get("createdAt")).getTime())); 
			cs.setString(5, 	(String) tweet.get("text"));			
			cs.setDouble(6, 	(tweet.get("geoLocationLatitude")!=null?(Double) tweet.get("geoLocationLatitude"):0));
			cs.setDouble(7, 	(tweet.get("geoLocationLongitude")!=null?(Double) tweet.get("geoLocationLongitude"):0)); 
			cs.setLong(8, 		(Long) tweet.get("retweetCount"));
			short tweetType = (Boolean) tweet.get("isRetweet")? (short) 1: //Retweet
				(Long) tweet.get("inReplyToStatusId") != null? (short) 2: //Reply
				(short) 0; //Original			
			cs.setShort(9,  	tweetType);
			cs.setString(10,	tweet.get("userName")!=null?(String) tweet.get("userName"):"");
			cs.setString(11,	tweet.get("source")!=null?(String) tweet.get("source"):"");
			cs.setLong(12,		tweet.get("retweetedStatusId")!=null?(Long)tweet.get("retweetedStatusId"):-1);
			cs.setLong(13,		tweet.get("retweetedUserId")!=null?(Long)tweet.get("retweetedUserId"):-1);
			cs.setString(14,	tweet.get("retweetedUserScreenName")!=null?(String)tweet.get("retweetedUserScreenName"):"");						
			cs.setLong(15,		(Long) tweet.get("inReplyToStatusId"));
			cs.setLong(16,		(Long) tweet.get("inReplyToUserId"));
			cs.setString(17,	tweet.get("inReplyToScreenName")!=null?(String)tweet.get("inReplyToScreenName"):"");
			cs.setBoolean(18,	(Boolean) tweet.get("isTruncated"));
			cs.setBoolean(19,	(Boolean) tweet.get("isFavorited"));
			if (tweet.get("isPossiblySensitive") != null) {
				cs.setBoolean(20,	(Boolean) tweet.get("isPossiblySensitive"));
			} else {
				cs.setBoolean(20,	false);
			}
			
			
			cs.execute();
			
			/*
			if(tweet.getRetweetedStatus()!=null){
				saveTweet(tweet.getRetweetedStatus(), host, port, dbName, user, password);
			}
			
			saveUserProfile(tweet, host, port, dbName, user, password);
			*/

		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage() + " \n " + ex.getStackTrace().toString());
			conn = null;
		}
	}
	
	public static void saveUserProfile(Map tweeter, String host, int port, String dbName, String user, String password) {
		try {

			Connection cnx = getConnection(host, port, dbName, user, password);
			if(cnx==null || tweeter==null)
				return;
			
			CallableStatement cs = cnx.prepareCall("{call TWITTERUSERPROFILE(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cs.setLong(1, 		(Long) tweeter.get("id")); 
			cs.setLong(2, 		(Long) tweeter.get("userId"));
			cs.setString(3, 	(String) tweeter.get("userScreenName")); 	
			cs.setTimestamp(4, 	new java.sql.Timestamp(((Date) tweeter.get("createdAt")).getTime()));
			cs.setTimestamp(5, 	new java.sql.Timestamp(((Date) tweeter.get("userCreatedAt")).getTime()));
			cs.setString(6, 	(String) tweeter.get("userDescription"));
			cs.setString(7, 	(String) tweeter.get("userLang"));
			cs.setString(8, 	(String) tweeter.get("userLocation"));
			cs.setInt(9, 	(Integer) tweeter.get("userFavouritesCount"));
			cs.setInt(10, 	(Integer) tweeter.get("userFollowersCount"));
			cs.setInt(11, 	(Integer) tweeter.get("userFriendsCount"));
			cs.setString(12, 	(String) tweeter.get("userName"));
			cs.setString(13, 	(String) tweeter.get("userTimeZone"));
			cs.setInt(14, 	(Integer) tweeter.get("userStatusesCount"));
			
			cs.execute();

		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage() + " \n " + ex.getStackTrace().toString());
			conn = null;
		}
	}
	
	public static void saveHashtags(long tweetId, String[] hashArray, String host, int port, String dbName, String user, String password){
		try{ 
			for(String entity : hashArray){
				Connection cnx = getConnection(host, port, dbName, user, password);
				if(cnx==null || hashArray.length == 0)
					return;
				
				CallableStatement cs = cnx.prepareCall("{call TWITTER_HASHTAG(?,?)}");				
				cs.setLong(1, 	tweetId); 
				cs.setString(2, entity); 
				cs.execute();
				cs.close();				
			}
		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage() + " \n " + ex.getStackTrace().toString());
			conn = null;
		}
	}

	public static void saveTweetURLs(long tweetId, String[][] urlArray, String host, int port, String dbName, String user, String password){
		try{ 
			for(String[] entity : urlArray){
				Connection cnx = getConnection(host, port, dbName, user, password);
				if(cnx==null || urlArray.length == 0)
					return;
				CallableStatement cs = cnx.prepareCall("{call TWITTER_URL(?,?,?,?,?)}");
				
				cs.setLong(1,  tweetId); 
				cs.setString(2, entity[0]); //url
				if (entity[1] != null) { //extendedURL
					cs.setString(3, entity[1]);
				} else {
					cs.setString(3, entity[0]);
				}
			    cs.setString(4, "");    
			    cs.setString(5, null);
			    cs.execute();	
			    cs.close();
			}
		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage() + " \n " + ex.getStackTrace().toString());
			conn = null;
		}	
	}

	public static void saveTweetMentions(long tweetId, String[][] userArray, String host, int port, String dbName, String user, String password){		
		for(String[] entity : userArray){
			Connection cnx = getConnection(host, port, dbName, user, password);
			if(cnx==null || userArray.length == 0)
				return;
			
			try{ 
				CallableStatement cs = cnx.prepareCall("{call TWITTER_MENTIONS(?,?,?,?)}");
					cs.setLong(1, 	tweetId); 
					cs.setLong(2,   Long.parseLong(entity[0])); //id
					cs.setString(3, entity[1]); //screen name
					cs.setString(4, entity[2]);	//name		
					cs.execute();			 
				cs.close();
			}catch(Exception ex){
				ex.printStackTrace();
				System.out.println(ex.getMessage() + " \n " + ex.getStackTrace().toString());
				conn = null;
			}
		}
	}
	
	public static void saveTrendingTopic(Map tt, String host, int port, String dbName, String user, String password){		
		System.out.println("--------------------- save Trending Topic:" + tt);
		Connection cnx = getConnection(host, port, dbName, user, password);
		if(cnx==null)
			return;
		
		try{ 
			PreparedStatement ps = cnx.prepareStatement("Insert Into TrendingTopics (name, woe_id, as_of, created_at) values (?, ?, ?, ?)");
				ps.setString(1, 	tt.get("name").toString()); 
				ps.setString(2, 	tt.get("woe_id").toString()); 
				ps.setLong(3, 	(Long) tt.get("as_of_ts"));
				ps.setLong(4, 	(Long) tt.get("created_at_ts"));
				ps.execute();			 
			ps.close();
		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage() + " \n " + ex.getStackTrace().toString());
			conn = null;
		}
	}
}
