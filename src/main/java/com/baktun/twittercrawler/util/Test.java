package com.baktun.twittercrawler.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Test {

	public static void main(String[] args) throws Exception {
		DateFormat DF = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
		DF.parse("Wed Aug 27 05:00:11 +0000 2014");
		
		DateFormat DFISO = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		DFISO.parse("2014-09-05T00:08:45Z");
	}

}
