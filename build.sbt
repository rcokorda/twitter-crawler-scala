import com.typesafe.sbt.SbtAspectj._

name := """twitter-crawler-scala"""

version := "1.0"

scalaVersion := "2.11.2"

val kamonVersion = "0.3.4"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.3.3",
  "com.typesafe.akka" %% "akka-testkit" % "2.3.3",
  "org.scalatest" %% "scalatest" % "2.1.6" % "test",
  "junit" % "junit" % "4.11" % "test",
  "com.novocode" % "junit-interface" % "0.10" % "test",
  "com.lambdaworks" %% "jacks" % "2.3.3",
  "org.mongodb" %% "casbah" % "2.7.2",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
  "nl.grons" %% "metrics-scala" % "3.2.1_a2.3",
  "io.kamon" %% "kamon-core" % kamonVersion,
  "io.kamon" %% "kamon-statsd" % kamonVersion,
  "io.kamon" %% "kamon-log-reporter" % kamonVersion,
  "io.kamon" %% "kamon-system-metrics" % kamonVersion,
  "org.aspectj" % "aspectjweaver" % "1.8.1"
)

aspectjSettings

javaOptions <++= AspectjKeys.weaverOptions in Aspectj

fork in run := true

testOptions += Tests.Argument(TestFrameworks.JUnit, "-v")